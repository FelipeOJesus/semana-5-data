import static org.junit.Assert.*;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class TesteDataDeNascimento {
	
	Pessoa p;
	Date dataDeNascimento;
	
	@Before
	public void inicializador() {
		p = new Pessoa();
		
	}

	@Test
	public void capricornio() {
		dataDeNascimento = new Date(95, 11, 25);
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Capric�rnio", p.getSigno());
	}
	
	@Test
	public void aquario() {
		dataDeNascimento = new Date(118, 0, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Aqu�rio", p.getSigno());
	}
	
	@Test
	public void peixes() {
		dataDeNascimento = new Date(118, 1, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Peixes", p.getSigno());
	}
	
	@Test
	public void aries() {
		dataDeNascimento = new Date(118, 2, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("�ries", p.getSigno());
	}
	
	@Test
	public void touro() {
		dataDeNascimento = new Date(118, 3, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Touro", p.getSigno());
	}
	
	@Test
	public void gemeos() {
		dataDeNascimento = new Date(118, 4, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("G�meos", p.getSigno());
	}
	
	@Test
	public void cancer() {
		dataDeNascimento = new Date(118, 5, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("C�ncer", p.getSigno());
	}
	
	@Test
	public void leao() {
		dataDeNascimento = new Date(118, 6, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Le�o", p.getSigno());
	}
	
	@Test
	public void virgem() {
		dataDeNascimento = new Date(118, 7, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Virgem", p.getSigno());
	}
	
	@Test
	public void libra() {
		dataDeNascimento = new Date(118, 8, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Libra", p.getSigno());
	}
	
	@Test
	public void escorpiao() {
		dataDeNascimento = new Date(118, 9, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Escorpi�o", p.getSigno());
	}
	
	@Test
	public void sargitario() {
		dataDeNascimento = new Date(118, 10, 25);
		
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals("Sagit�rio", p.getSigno());
	}
	
	@Test
	public void idade() {
		dataDeNascimento = new Date(95, 9, 13);
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals(22, p.getIdade());
		
	}
	
	@Test
	public void idadeNaDataDoTeste() {
		dataDeNascimento = new Date(95, 7, 15);
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals(23, p.getIdade());
	}
	
	@Test
	public void idadeUmDiaDepoisDaDataAtual() {
		dataDeNascimento = new Date(95, 7, 16);
		p.setDataDeNascimento(dataDeNascimento);
		assertEquals(22, p.getIdade());
		
	}
	
	

}
