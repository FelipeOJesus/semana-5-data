import java.util.Calendar;
import java.util.Date;

public class Pessoa {

	private Date dataDeNascimento;
	private int idade;
	private String signo;

	public int getIdade() {
		Calendar dataNascimento = Calendar.getInstance();
		dataNascimento.setTime(dataDeNascimento);
		Calendar dataAtual = Calendar.getInstance();

		idade = dataAtual.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR);

		if (dataNascimento.get(Calendar.MONTH) > dataAtual.get(Calendar.MONTH)) {
			idade --;
			return idade --;
		}if (dataNascimento.get(Calendar.DAY_OF_MONTH) > dataAtual.get(Calendar.DAY_OF_MONTH)) {
			idade--;
			return idade--;
		}
		return idade;
	}

	public String getSigno() {

		if (dataDeNascimento.after(new Date(dataDeNascimento.getYear(), 11, 22))
				&& dataDeNascimento.before(new Date(dataDeNascimento.getYear() + 1, 0, 21)))
			return "Capric�rnio";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 1, 19)))
			return "Aqu�rio";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 2, 20)))
			return "Peixes";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 3, 20)))
			return "�ries";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 4, 20)))
			return "Touro";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 5, 20)))
			return "G�meos";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 6, 21)))
			return "C�ncer";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 7, 22)))
			return "Le�o";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 8, 22)))
			return "Virgem";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 9, 22)))
			return "Libra";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 10, 21)))
			return "Escorpi�o";
		if (dataDeNascimento.before(new Date(dataDeNascimento.getYear(), 11, 21)))
			return "Sagit�rio";

		return signo;

	}

	public void setDataDeNascimento(Date dataDeNascimento) {
		this.dataDeNascimento = dataDeNascimento;
	}

}
